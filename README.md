# NessZilla #

Nesszilla is a integrated product of Nessus and Bugzilla. It is divided into three major part, Nessus, Bugzilla and third the integration.

## Nessus ##

Nessus is an open-source network vulnerability scanner that uses the Common Vulnerabilities and Exposures architecture for easy cross-linking between compliant security tools. Nessus employs the Nessus Attack Scripting Language (NASL), a simple language that describes individual threats and potential attacks. 


## Steps in setting up Nessus ##
### Step 1###

#### Gather Pre-requisite
* Nessus Api : : https://centauros.intimetec.americas
* Python requests module
* Python urllib3 module
* Python json module

### Step 2 ###

#### Store the username and password in environment variable
* Reference : : https://www.youtube.com/watch?v=IolxqkL7cD8&t=215s 

```sh
	username = os.environ.get('bg_username')
	password = os.environ.get('bg_password')
```
### Step 3 ###

#### Create a Session at Nessus
* Create a session at Nessus it will require username and password and a call to Nessus API.
* Session creation url : https://centauros.intimetec.americas/session
* Send a request to the API with python requets module
```sh
	myobj = {"username": username, "password": password}
    session_creation = requests.post(session_api, data=myobj, verify=False)
```
* The response of the requests will contain the token.
```sh
	{
	token:"zfhksufhisfblji837ryhwff8nwc8ndy8n3y45dny3fn98ymf8948ny9"
	}
```
* Convert the webresponse to json with json.loads or create a seprate function to convert webresponse to json. Here I make a json parser function.
```sh
def json_parser(web_response):
    json_parsed_data = json.loads(web_response.text)
    return json_parsed_data
```
* Store this token in header as "X-Cookie"

### Step 4 ###

#### Grabbing all scan

* Grab all the scans present on Nessus
* Scan url:https://centauros.intimetec.americas/scans
* Change the response of web into json with json_parser function.
```sh
	all_scan_response = requests.get(url=scan_url, headers=header, verify=False)
    all_scan_data = json_parser(all_scan_response)
```
* The response will provide details of all the scans available at Nessus whether it is in myscan folder or test folder.
* The main scan folder is myscan folder. Its folder id is 3.
* The scan present in the myscan folder are of main focus.

### Step 5 ###

#### Checking the unread scans

* Reponse of step 4 contain each scan and in each scan there is a field "read". Check this field for each scan and if the field is false store its id into a list.
```sh
    for scan in total_scans:
        if scan["folder_id"] == 3 and scan["read"] == False:
            unread_scan_list.append(scan["id"])
```
* Now scans are more filtered and these scan are of main focus now.

### Step 6 ###

#### Examine each scan

* After grabbing the required scan IDs, check them if they have some high and critical vulnerabilities.
* Scan details URL: https://centauros.intimetec.americas/scans/{scan_id} 
* Get each scan web response and convert it into json.
* Log this scan information to the log file.
* Check each json response for high and critical vulnerability. If the high and critical vulnerabilty count is greater then 0 its time to export the report.
```sh
		print("Server Name : ", scanned_json_report["info"]["name"], end='\n\n')
        logging.info("Server Name : "+str(scanned_json_report["info"]["name"]))
        print("Critical : ", scanned_json_report["hosts"][0]["critical"])
        logging.info("Critical : "+str(scanned_json_report["hosts"][0]["critical"]))
        print("Offline-Critical : ", scanned_json_report["hosts"][0]["offline_critical"], end='\n\n')
        logging.info("Offline-Critical : "+str(scanned_json_report["hosts"][0]["offline_critical"]))
        print("High : ", scanned_json_report["hosts"][0]["high"])
        logging.info("High : "+str(scanned_json_report["hosts"][0]["high"]))
        print("Offline-High :", scanned_json_report["hosts"][0]["offline_high"], end='\n\n')
        logging.info("Offline-High :"+str(scanned_json_report["hosts"][0]["offline_high"]))
        print("Medium : ", scanned_json_report["hosts"][0]["medium"])
        logging.info("Medium : "+str(scanned_json_report["hosts"][0]["medium"]))
        print("Offline Medium : ", scanned_json_report["hosts"][0]["offline_medium"], end='\n\n')
        logging.info("Offline Medium : "+ str(scanned_json_report["hosts"][0]["offline_medium"]))
        print("Low : ", scanned_json_report["hosts"][0]["low"])
        logging.info("Low : "+ str(scanned_json_report["hosts"][0]["low"]))
        print("Offline-Low : ", scanned_json_report["hosts"][0]["offline_low"], end='\n\n')
        logging.info("Offline-Low : "+str(scanned_json_report["hosts"][0]["offline_low"]))
        print("Informartional : ", scanned_json_report["hosts"][0]["info"])
        logging.info("Informartional : "+str(scanned_json_report["hosts"][0]["info"]))
        print("Offline-Informartional : ", scanned_json_report["hosts"][0]["offline_info"], end='\n\n')
        logging.info("Offline-Informartional : "+ str(scanned_json_report["hosts"][0]["offline_info"]))
```

### Step 6 ###

#### Raise a export report request

* To get the report first we have to create a request to Nessus.
* The request include the report format and the parameter we want in our report.
* This data is send as payload to the request.
```sh
    report_format = "pdf"
    payload = {"format": report_format,
               "reportContents.formattingOptions.page_breaks": True,
               "reportContents.vulnerabilitySections.description": True,
               "reportContents.hostSections.scan_information": True,
               "reportContents.hostSections.host_information": True,
               "chapters": 'vuln_hosts_summary; vuln_by_host; compliance_exec; remediations; vuln_by_plugin, compliance;',
               "reportContents.vulnerabilitySections.synopsis0": True,
               "reportContents.vulnerabilitySections.plugin_information": True
               }
```
* export_request_url = https://centauros.intimetec.americas/scans/{scan_id}/export
* Make a request
```sh
   export_request_response = requests.post(url=export_request_url, headers=header, data=payload, verify=False)
```
* It will respond by giving back a token and a file id these file id can be only used once to download the report. To check the status it can be used any number of time.

### Step 7 ###

#### Check the status of report

* Check status url : https://centauros.intimetec.americas/scans/{scan_id}/export/{file_id}/status
* Check the status of the exported report, if the status of the report is loading, sleep python program for 5 second.
* The other alternative is to put the checking status request in loop and once the report is ready, the report is ready to download.
```sh
    while export_status_json["status"] == "loading":
        export_status_url = 'https://centauros.intimetec.americas/scans/' + str(scan_id) + '/export/' + str(file_id) + '/status'
        export_status_response = requests.get(url=export_status_url, headers=header, verify=False, timeout=10)
```

### Step 8 ###

#### Download the report 

* Report download url:https://centauros.intimetec.americas/scans/{scan_id}/export/{file_id}/download
* Download the report.
* The report will be still in web response convert into in pdf file and store into local.
```sh
def pdf_creater(server_name,report):
    file=open("report.pdf",'wb')
    file.write(report.content)
    file.close()
```

## Bugzilla ##

Bugzilla is a web-based general-purpose bug tracking system and testing tool originally developed and used by the Mozilla project, and licensed under the Mozilla Public License.


## Steps in setting up Nessus ##
### Step 1###

#### Gather Pre-requisite
* Install python-bugzilla==2.4.0

### Step 2 ###

#### Login at bugzilla

* Login with bugzilla credential
* Intimetec Bugzilla API : https://bugzilla.intimetec.americas
```sh
 URL = "https://bugzilla.intimetec.americas"
    username = os.environ.get('bg_username')
    password = os.environ.get('bg_password')
    bz = bugzilla.Bugzilla44(url=URL, sslverify=False, user=username, password=password)
```
* Now this 'bz' object will handle all of the rest operation.

### Step 3 ###

#### Creating the bug info

* Create a bug info with build_createbug funtion of the bugzilla library.
```sh
create_info = bz.build_createbug(
            product="IT Department",
            version="unspecified",
            component="Servers/hosting",
            summary="Nessus Scan on " + server_name + " has some high and critical issues.",
            description="Nessus scan has identified high and critical issues in " + server_name + ".It is advised by Cyber Security Team to resolve these issues.",
            op_sys="Windows",
            cc="CyberSecurityTeam@intimetec.com",
            platform="pc"
        )
```
* These are the required parameter for these function. If any less is supplied it will cause an error.

### Step 4 ###

#### Creating the bug

* As what info to be stores into bug is created, Now Its time to create the bug.
* To create the bug use the fucntion create_bug
```sh
   new_bug = bz.createbug(create_info)
```

### Step 5 ###
 
#### Attaching the report

* To attach the downloaded report use attach report function. 
```sh
    bz.attachfile(idlist=[bug_id],attachfile="report.pdf",description="Nessus Scan Report.")
```

## NessZilla ##

#### Integrate Nessus and Bugzilla

* As the Nessus and Bugzilla part is done its time to integrate them. This can be done in two ways either make bugzilla task as a function in the Nessus script or make a bugzilla script as a separate one and import this as module in Nessus script.

#### To not raise a bug again for the same server if already a bug raised

* To do so store the last raised bug for the server as key value pair into a dict.
* Store this dict into a file with the pickle python library.

#### Additional Functionality

* Store everything whatever the script doing into a log file with python logging module.
* Store raised bug id into a file.
