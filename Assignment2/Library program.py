
import pickle


class normal():
    def __init__(self,name,password):
        self.name=name;
        self.password=password
        self.rent_count=0
        self.rent_book_list=[]
        
    def show_available_books(self):
        available_books_file=open("book.txt","rb")
        available_books_list=pickle.load(available_books_file)
        #print(type(available_books_list))
        #print("The available book list")
        count=1
        for i in available_books_list:
            #count=1
            #if(available_books_list[i]!=0):
            print(count, ". ",i, " : ", available_books_list[i])
            count=count + 1 
    
    def show_rent_books(self):
        print("Number of the books rent by you : ", self.rent_count)
        
        if(len(self.rent_book_list)==0):
            print("There is no book issued by you")
        else:
            print("These are your rented books")
            for i in self.rent_book_list:
                print(" ++> ",i)
        
        
    def issue_book(self):
        print("Enter the name of the book you want to issue (Case-Sensitive)")
        self.show_available_books()
        user_choice=input()
        
        available_books_file=open("book.txt","rb")
        available_books_list=pickle.load(available_books_file)
        available_books_file.close()
        
        available_books_list[user_choice]-=1
        self.rent_count+=1
        self.rent_book_list.append(user_choice)
        

        available_books_file=open("book.txt","wb")
        pickle.dump(available_books_list,available_books_file)
        available_books_file.close()
        
    
    def return_book(self):
        print("Enter the name of the book you want to return (Case-Sensitive)")
        user_input=input()
        
        available_books_file=open("book.txt","rb")
        available_books_list=pickle.load(available_books_file)
        available_books_file.close()
        
        available_books_list[user_input]+=1
        self.rent_count-=1
        self.rent_book_list.remove(user_input)
        
        available_books_file=open("book.txt","wb")
        pickle.dump(available_books_list,available_books_file)
        available_books_file.close()

            
# some pre required function to create files

admin=normal("admin","admin")
object_list=[]
object_list.append(admin)
object_file=open("objects.txt","wb")
pickle.dump(object_list,object_file)
object_file.close()


book_file=open("book.txt","wb")
key1="Great Expectation"
key2="War and Peace by Leo Tolstoy"
key3="Madame Bovary"
dict1={
    key1:5,
    key2:5,
    key3:5
}
#print(dict1)
pickle.dump(dict1,book_file)
book_file.close()



def authentication(username_input,password_input):
    object_file=open("objects.txt","rb")
    object_list=pickle.load(object_file)
    object_file.close()
    
    object_file=open("objects.txt","wb")
    pickle.dump(object_list,object_file)
    object_file.close()
    
    #print(object_list)

    for obj in object_list:
        #print(obj.name)
        #print(obj.password)
        if(obj.name==username_input):
            if(obj.password==password_input):
                return obj
            else:
                return False
        else:
            pass


def main():
    print("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++", end="\n")
    print("Welcome to the ITT library", end="\n")
    
    print("Choose your prefrence", end="\n")
    print("1. Existing User Login")
    print("2. New user registration")
    
    user_choice=int(input())
    if(user_choice==1):
        print("Enter your username")
        username_input=input()
        print("Enter the password")
        password_input=input()
        
        current_user=authentication(username_input,password_input)
        
        if(current_user==False):
            print("You are not Registered \n")
            main()
        
        else:
            print("Login succcessfully")
            logout=False
            while(logout==False):
                
                print("\n")
                print("Enter your choice number \n")
                print("1:Display Available Books In Library")
                print("2:Book I Have")
                print("3:Issue New Book")
                print("4:Return Book")
                print("5:Logout")

                user_input=int(input())
                if(user_input==1):
                    current_user.show_available_books()
                elif(user_input==2):
                    current_user.show_rent_books()
                elif(user_input==3):
                    current_user.issue_book()
                elif(user_input==4):
                    current_user.return_book()
                elif(user_input==5):
                    print("_________________________________________________________________________________________________")
                    print("Visit the Library again")
                    print("_________________________________________________________________________________________________")
                    logout=True
                else:
                    print("Wrong Input")

    elif(user_choice==2):
        
        print("Enter the username")
        username=input()
        print("Enter the password")
        password=input()
        
        new_object=normal(username,password)
        
        object_file=open("objects.txt","rb")
        object_list=pickle.load(object_file)
        object_list.append(new_object)
        object_file.close()
        
        object_file=open("objects.txt","wb")
        pickle.dump(object_list,object_file)
        print(object_list)
        object_file.close()
        
        print("You are successfully registered")
        print("You can login now \n ")
        main()
        
        
        
main()





