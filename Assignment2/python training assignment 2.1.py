#!/usr/bin/env python
# coding: utf-8

# In[3]:


#Write a method to take list as an input and gives output as list of tuples. where each tuple will contain (index, value).

def input_list():
    print("Enter the list elements")
    list1=list(map(int,input().split(" ")))
    return list1

def list_to_list_of_tuple(list1):
    index=0
    new_list=[]
    for i in list1:
        tuple1=(index,i)
        new_list.append(tuple1)
        index+=1
    return new_list

def output_newlist(modified_list):
    print("New List : ",modified_list)
    
def main():
    entered_list=input_list()
    list_of_tuples=list_to_list_of_tuple(entered_list)
    output_newlist(list_of_tuples)

main()


# In[ ]:




