#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pickle
import sys


# In[2]:

# Pre required to create user file
file=open("user.txt","wb")
dict1={"aakash":"12345"}
pickle.dump(dict1,file)
file.close()


# In[3]:

# Pre required to create rent file
file=open("rent.txt","wb")
dict1={"aakash":0}
pickle.dump(dict1,file)
file.close()


# In[4]:

# Pre required to create books file
book_file=open("book.txt","wb")
key1="Great Expectation"
key2="War and Peace by Leo Tolstoy"
key3="Madame Bovary"
dict1={
    key1:5,
    key2:5,
    key3:5
}
print(dict1)
pickle.dump(dict1,book_file)
book_file.close()


# In[5]:

# Main Function
import pickle
def main():
    print("Welcome to ITT library.")
    print("Choose your prefrence")
    print("1. Existing User Login")
    print("2. New user registration")
    
    user_choice=int(input())
    
    if user_choice==1:
        print("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
        print("Enter the username")
        username=input()
        username_file=open("user.txt", "rb")
        username_dict = pickle.load(username_file)
        username_file.close()
        if username in username_dict:
            password=input("Enter the password")
            #password=input()
            if(password == username_dict[username]):
                login=True
                while(login):
                    print("Login succcessfully")
                    print("1:Display Available Books In Library")
                    print("2:Book I Have")
                    print("3:Issue New Book")
                    print("4:Return Book")
                    print("5:Quit")
                    user_choice=int(input("Enter you choice "))
                    if(user_choice==1):
                        available_books_file=open("available_books.txt","rb")
                        available_books_list=pickle.load(available_books_file)
                        print("The available book list")
                        for i in available_books_list:
                            print("--> ",i)

                    elif(user_choice==2):
                        rent_file=open("rent.txt","rb")
                        rent_dict=pickle.load(rent_file)
                        print("These are the no of books rent by you.")
                        print(rent_dict[username])

                    elif(user_choice==3):
                        print("Issue new book")
                        print("The following book is available")
                        available_books_file=open("book.txt","rb")
                        available_book_dict=pickle.load(available_books_file)
                        for key, value in available_book_dict.items():
                            if value != 0:
                                print(key)
                        print("Enter the name of the book you want to buy")
                        want_book=input()
                        available_books_file.close()
                        available_books_file=open("book.txt","wb")
                        #available_book_dict=pickle.load(available_books_file)
                        available_book_dict[want_book] -= 1
                        pickle.dump(available_book_dict,available_books_file)
                        available_books_file.close()
                        rent_file=open("rent.txt","rb")
                        rent_dict=pickle.load(rent_file)
                        rent_file.close()
                        rent_dict[username] += 1
                        rent_file=open("rent.txt","wb")
                        pickle.dump(rent_dict,rent_file)
                        rent_file.close()
                    elif(user_choice==4):
                        print("Enter the name of the book you need to return")
                        return_book=input()
                        available_books_file=open("book.txt","rb")
                        available_book_dict=pickle.load(available_books_file)
                        available_books_file.close()
                        available_books_file=open("book.txt","wb")
                        available_book_dict[return_book] += 1
                        pickle.dump(available_book_dict,available_books_file)
                        available_books_file.close()
                        rent_file=open("rent.txt","rb")
                        rent_dict=pickle.load(rent_file)
                        rent_dict[username] -= 1
                        rent_file.close()
                        rent_file=open("rent.txt","wb")
                        pickle.dump(rent_dict,rent_file)
                        rent_file.close()
                    elif(user_choice ==5):
                        login=False
                        sys.exit("Thanks for visiting the library")
                    else:
                        pass
            else:
                print("Wrong password")
        else:
            print("Invalid username")
            
            
            
    elif user_choice==2:
        print("Enter the username to register")
        new_username=input()
        print("Enter your password")
        new_password=input()
        username_file=open("user.txt", "rb")
        username_dict = pickle.load(username_file)
        username_file.close()
        username_dict[new_username]=new_password
        username_file=open("user.txt", "wb")
        pickle.dump(username_dict,username_file)
        print("User registered successfully")
        username_file.close()
        rent_file=open("rent.txt","rb")
        rent_dict=pickle.load(rent_file)
        rent_file.close()
        rent_file=open("rent.txt","wb")
        rent_dict[new_username] = 0
        rent_file.close()
        print("Rent updated")
    
    


# In[6]:


main()


# In[ ]:




