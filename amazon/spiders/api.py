from flask import Flask
from flask_restful import Resource, Api
from sqlalchemy import create_engine
from flask import jsonify

app = Flask(__name__)
api = Api(app)

db_connect = create_engine('sqlite:///amazon.db')

# To get the all authors name
class Authors(Resource):
    def get(self):
        conn = db_connect.connect()  # connect to database
        query = conn.execute("select * from amazon_table2")
        return {'author': [i[2] for i in query.cursor.fetchall()]}


api.add_resource(Authors, '/authors')  # Route_1

#To get the Whole Data
class Data(Resource):
    def get(self):
        conn = db_connect.connect()
        query = conn.execute("select * from amazon_table2")
        result = {'data': [dict(zip(tuple(query.keys()), i)) for i in query.cursor]}
        return jsonify(result)

api.add_resource(Data, '/data')  # Route_2

#To get the books till perticular serial number
class Books(Resource):
    def get(self, serial_no):
        conn = db_connect.connect()
        query = conn.execute("select * from amazon_table2 where Number <=%d " % int(serial_no))
        result = {'data': [dict(zip(tuple(query.keys()), i)) for i in query.cursor]}
        return jsonify(result)

api.add_resource(Books,'/books-till/<serial_no>')  # Route_3

if __name__ == '__main__':
    app.run(port='5000')