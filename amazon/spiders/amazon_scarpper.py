import scrapy
import sys
sys.path.insert(1, r'C:\Users\aakash.rathee\PycharmProjects\amazon\amazon\amazon')

from items import AmazonItem

class amazon(scrapy.Spider):
    name = "books"
    #url="https://www.amazon.in/s?k=ac&ref=nb_sb_noss_2"
    #user_input=str(input("Enter the keyword you want to search"))
    main_url="https://www.amazon.in/s?k=books&ref=nb_sb_noss"
    start_urls = [main_url]
    #page_number=2
    def parse(self, response):

        items = AmazonItem()
        count=0

        title=response.css('.a-color-base.a-text-normal').css('::text').extract()
        price=response.css('.a-spacing-top-small .a-price-whole').css('::text').extract()
        author=response.css('.a-color-secondary .a-size-base:nth-child(2)').css('::text').extract()

        items['Title']=title
        items['Price']=price
        #items['Author']=review_count
        items['Author']=author

        for i in range(len(items['Author'])):
           items['Author'][i]=items['Author'][i][36:-15]

        yield items

            #next_page='http://quotes.toscrape.com/page/'+ str(self.page_number)+'/'
            #if next_page is not None:
            #   self.page_number+=1
            #  yield response.follow(next_page, callback=self.parse)
