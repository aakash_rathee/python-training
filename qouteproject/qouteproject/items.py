# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class QouteprojectItem(scrapy.Item):
    # define the fields for your item here like:
    Title = scrapy.Field()
    Author = scrapy.Field()
    Word = scrapy.Field()
    Tag = scrapy.Field()
    
