# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html

import sqlite3

class QouteprojectPipeline:
    def __init__(self):
        self.create_connection()
        self.create_table()

    def create_connection(self):
        self.conn=sqlite3.connect("mydatabase.db")
        self.curr=self.conn.cursor()

    def create_table(self):
        self.curr.execute(""" drop table if exists mytable1""")
        self.curr.execute("""create table mytable1 (Title text, Author text, Word test, Tag text)""")
    
    def process_item(self, item, spider):
        self.store_db(item)
        return item

    def store_db(self,item):
        self.curr.execute(""" insert into mytable1 values(?,?,?,?)""",(item['Title'][0],item['Author'][0],item['Word'][0],item['Tag'][0]))
        self.conn.commit()
