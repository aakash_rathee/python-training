#!/usr/bin/env python
# coding: utf-8

# In[7]:


import scrapy
from scrapy import Spider

# In[11]:
import sys

sys.path.insert(1, r'E:\python-training\qouteproject\qouteproject')
from items import QouteprojectItem

class qoutesClass(scrapy.Spider):
    name="qoutes"
    start_urls=['http://quotes.toscrape.com/']
 
    def parse(self, response):
        div_text = response.css('div.quote')
        items = QouteprojectItem()

        for qoutes in div_text:
            word=qoutes.css('span.text::text').extract()
            title=response.css('title::text').extract()
            author=qoutes.css('.author::text').extract()
            tag=qoutes.css('.tag::text').extract()

            items['Title']=title
            items['Word']=word
            items['Author']=author
            items['Tag']=tag

            yield items


# In[ ]:




