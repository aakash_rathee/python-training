#!/usr/bin/env python
# coding: utf-8

# In[36]:


# Write a program to sort list, dictionary. 

list1=list(map(int,input().split(" ")))
list1.sort()
print("Sorted List")
print(list1)

dictionary={
    "student":"Aakash",
    "technology":"CS",
    "college":"ACEIT"
}

print("Sorted Dictionary on the basis of Keys")
print(sorted(dictionary.keys()))
print("Sorted Dictionary on the basis of Values")
print(sorted(dictionary.values()))

