#!/usr/bin/env python
# coding: utf-8

# In[ ]:


# Write a program to remove duplicate elements from given list with original order.

print("Enter the Number of the elements in list")
n=int(input())

list1=[]
for i in range(0,n):
    list1.append(int(input()))
    
new_list=[]

for i in list1:
    if i not in new_list:
        new_list.append(i)

        
print("New list with no repeated elements")
print(new_list)


# In[ ]:




