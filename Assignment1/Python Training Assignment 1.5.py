#!/usr/bin/env python
# coding: utf-8

# In[1]:


#Write a program to print all prime number between 0, 10 using list comprehension.


prime_number=[var for var in range(0,11) if var%2==0]

print(prime_number)


# In[ ]:




