#!/usr/bin/env python
# coding: utf-8

# In[51]:


import json
import sys
import requests

def api_hitting(lattitude,longitude):
    url="https://"
    parameters = {"lat":lattitude, "lon":longitude, "appid":"20227699189326892e684ba73cfe855a"}
    main_api = "https://api.openweathermap.org/data/2.5/weather"
    response = requests.get(url=main_api, params=parameters)
    required_data = json.loads(response.text)
    return required_data

def weather_telecast(required_data):
    print()
    print("Place : ",required_data["name"])
    print("Country : ", required_data["sys"]["country"])
    print("Weather: ", required_data["weather"][0]['main'])
    print("Description :",required_data["weather"][0]['description'])
    
def user_input():
    coordinates=[]
    print("Enter the Lattitude")
    coordinates.append(float(input()))
    print("Enter the Longitude")
    coordinates.append(float(input()))
    return coordinates

def main():
    print("Welcome to ITT weather cast!!!", end="\n\n")
    coordinates=user_input()
    lat=coordinates[0]
    lon=coordinates[1]
    required_data=api_hitting(lat, lon)
    weather_telecast(required_data)

main()


# In[ ]:




